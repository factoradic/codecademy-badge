# codecademy-badge

Proof of concept for https://discuss.codecademy.com/t/share-my-profile-in-my-website/410175 discussion

## Instructions

```
npm install
node index.js
```

Visit http://localhost:5000/{codecademy_username}
