const captureWebsite = require('capture-website');
const express = require('express');

const app = express();

const port = (process.env.PORT || 5000);

const options = {
    launchOptions: {
        args: ['--no-sandbox', '--disable-setuid-sandbox'],
    },
    fullPage: true,
    overwrite: true,
    styles: ['style.css'],
    width: 600,
    height: 200,
    scaleFactor: 1
};

app.get('/:user', async (req, res, next) => {
    try {
        let userName = req.params.user;
        await captureWebsite.file('https://www.codecademy.com/' + userName, userName + '.png', options);
        res.sendFile(__dirname + '/' + userName + '.png');
    } catch (e) {
        next(e);
    }
});

app.listen(port, () => console.log(`Codecademy-badge app listening on port ${port}!`));
